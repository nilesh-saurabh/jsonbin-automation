package stepdefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import javatestClass.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StepDefinition_jsonbin {


  public String hostName;
  public String loadContent;
  public Integer statusCode;
  public String content;
  public String jsonBinId;
  public static String jsonbinID;
  public static String jsonbinIDNew;
  public static Jsonbin_inputDTO createJsonbinInputJSONObj;
  public static Updated_Jsonbin_inputDTO updateJsonbinInputJSONObj;
  public Response createJsonbinResponse;
  public Response getJsonbinResponse;
  public Response getJsonbinIdResponse;
  public Response getJsonbinLatestResponse;
  public Response updateJsonbinResponse;
  public Response deleteJsonbinResponse;
  public Response createJsonbinErrorResponse;
  public Response getJsonbinIdErrorResponse;
  public Response updateJsonbinErrorResponse;
  public Response deleteJsonbinErrorResponse;


// ==============================================================================================================================================================================================================================================================================================================================================

  @Given("^User should be able to hit jsonbin api with valid host name \"([^\"]*)\"$")
  public void User_should_be_able_to_hit_jsonbin_api_with_valid_host_name(
      String hostUrl) throws Throwable {

    hostName = hostUrl;

    String baseURL = hostUrl;
  }

// ========================================================================================================================================================================================================================================================================================================================================================
  // Create Jsonbin Service
  @When("^User create jsonbin service with content \"([^\"]*)\" and using host name \"([^\"]*)\"$")
  public void User_create_jsonbin_service_with_content_and_using_host_name(
      String Content, String hostUrl) throws Throwable {

    loadContent = Content;
    hostName = hostUrl;

    createJsonbinInputJSONObj = new Jsonbin_inputDTO();
    CreateJson createjsonbin = new CreateJson();
    createJsonbinResponse = createjsonbin.createJson(hostName, createJsonbinInputJSONObj);

    jsonbinID = createJsonbinResponse.jsonPath().get("metadata.id");

    if (createJsonbinResponse.getStatusCode() != 200) {
      System.out.println("createJsonbin failed");
    }
    else
      System.out.println("createJsonbin passed");
  }

// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify created jsonbin
  @Then("^User should be able to verify the created jsonbin in response body using content \"([^\"]*)\"$")
  public void User_should_be_able_to_verify_the_created_jsonbin_using_content(
      String Content) throws Throwable {

    loadContent = Content;
    assertNotEquals(createJsonbinResponse.jsonPath().getString("record.sample").toString(),"");
    String new_content = createJsonbinResponse.jsonPath().get("record.sample");
    assertEquals(Boolean.TRUE, createJsonbinResponse.getBody().asString().contains(Content));
    assertEquals(createJsonbinResponse.getStatusCode(),200);
  }

// ========================================================================================================================================================================================================================================================================================================================================================
  // Get Jsonbin Service
  @When("^User read jsonbin service using host name \"([^\"]*)\"$")
  public void User_should_read_jsonbin_service_using_host_name(
       String hostUrl) throws Throwable {

     hostName = hostUrl;

     GetJsonbin jsonbin = new GetJsonbin();
     getJsonbinResponse = jsonbin.getJsonbin(hostName);
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify getJsonbin service
  @Then("^User should be able to verify the jsonbin response using response message$")
  public void User_should_be_able_to_verify_the_jsonbin__response_using_response_message(
       ) throws Throwable {

     assertNotEquals(getJsonbinResponse.jsonPath().getString("message").toString(), "");
     assertEquals(getJsonbinResponse.getStatusCode(),200);
  }

// ========================================================================================================================================================================================================================================================================================================================================================
  // Get Jsonbin Service using Id
  @And("^User should read jsonbin api using jsonbin Id and host name \"([^\"]*)\"$")
  public void User_should_read_jsonbin_api_using_jsonbin_id_and_hostname(
    String hostUrl) throws Throwable {

    hostName = hostUrl;
    GetJsonbin jsonbinId = new GetJsonbin();
    getJsonbinIdResponse = jsonbinId.getJsonbinId(hostName, jsonbinID);
    assertNotEquals(getJsonbinIdResponse.jsonPath().getString("record.sample").toString(), "");
    assertEquals(getJsonbinIdResponse.getStatusCode(),200);
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Get latest Jsonbin Service using id
  @And("^User should read jsonbin api using jsonbin Id created latest and with host name \"([^\"]*)\"$")
  public void User_should_read_jsonbin_api_using_latest_jsonbin_id(
       String hostUrl) throws Throwable {

     hostName = hostUrl;
     GetJsonbin jsonbinlatest = new GetJsonbin();
     getJsonbinLatestResponse = jsonbinlatest.jsonbinLatest(hostName,jsonbinID);
     assertNotEquals(getJsonbinLatestResponse.jsonPath().getString("record.sample").toString(), "");
     assertEquals(getJsonbinLatestResponse.getStatusCode(),200);
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Update Jsonbin API Service
  @When("^User update jsonbin api with content and using host name \"([^\"]*)\"$")
  public void User_update_jsonbin_api_with_content_and_using_host_name(
       String hostUrl) throws Throwable {

     hostName = hostUrl;
     updateJsonbinInputJSONObj = new Updated_Jsonbin_inputDTO();
     UpdateJsonbin updatejsonbin = new UpdateJsonbin();
     updateJsonbinResponse = updatejsonbin.updateJsonbin(hostName, updateJsonbinInputJSONObj, jsonbinID );
     jsonbinIDNew = updateJsonbinResponse.jsonPath().get("metadata.parentId");
     assertNotEquals(updateJsonbinResponse.jsonPath().getString("record.sample").toString(), "");
     assertEquals(updateJsonbinResponse.jsonPath().get("metadata.parentId"),jsonbinID);

     if (updateJsonbinResponse.getStatusCode() != 200) {
       System.out.println("updateJsonbin failed");
     }
     else
       System.out.println("updateJsonbin passed");
  }

// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify updated jsonbin api
  @Then("^User should be able to verify the created jsonbin in response body using new request body as content \"([^\"]*)\"$")
  public void User_should_be_able_to_verify_the_updated_jsonbin_api_using_content(
       String Content) throws Throwable {

     content = Content;
     assertNotEquals(updateJsonbinResponse.jsonPath().getString("record.sample").toString(),"");
     assertEquals(Boolean.TRUE, updateJsonbinResponse.getBody().asString().contains(content));
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify error scenario for create jsonbin api
  @When("^User gets error while creating jsonbin service with content \"([^\"]*)\" and using host name \"([^\"]*)\"$")
  public void User_gets_error_while_creating_jsonbin_service_with_content_and_using_host_name(
           String Content, String hostUrl) throws Throwable {

       loadContent = Content;
       hostName = hostUrl;

       createJsonbinInputJSONObj = new Jsonbin_inputDTO();
       CreateJson createjsonbinerror = new CreateJson();
       createJsonbinErrorResponse = createjsonbinerror.createJson_error(hostName, createJsonbinInputJSONObj);

       if (createJsonbinErrorResponse.getStatusCode() != 400) {
           System.out.println("createJsonbinError failed");
       }
       else
           System.out.println("createJsonbinError passed");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify content for created jsonbin error
  @Then("^User should be able to verify the error in created jsonbin api using response body$")
  public void User_should_be_able_to_verify_the_error_in_created_jsonbin_using_response_body(
            ) throws Throwable {

        assertNotEquals(createJsonbinErrorResponse.jsonPath().getString("message").toString(),"");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify error scenario for get jsonbin api
  @And("^User gets error while reading jsonbin api using jsonbin Id \"([^\"]*)\" and host name \"([^\"]*)\"$")
  public void User_gets_error_while_reading_jsonbin_api_using_jsonbin_id_and_hostname(
            String jsonbinId, String hostUrl ) throws Throwable {

        hostName = hostUrl;
        jsonBinId = jsonbinId;
        GetJsonbin jsonbinIdError = new GetJsonbin();
        getJsonbinIdErrorResponse = jsonbinIdError.getJsonbinIdError(hostName, jsonBinId);

        if (getJsonbinIdErrorResponse.getStatusCode() != 404) {
            System.out.println("getJsonbinError failed");
        }
        else
            System.out.println("getJsonbinError passed");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify content for get jsonbin error
  @Then("^User should be able to verify the error in get jsonbin api using response body$")
  public void User_should_be_able_to_verify_the_error_in_get_jsonbin_using_response_body(
        ) throws Throwable {

        assertNotEquals(getJsonbinIdErrorResponse.jsonPath().getString("message").toString(),"");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify error scenario for update jsonbin api
  @When("^User gets error in updating jsonbin api with content and using host name \"([^\"]*)\"$")
  public void User_gets_error_while_updating_jsonbin_api_using_content_and_hostname(
            String hostUrl) throws Throwable {

        hostName = hostUrl;
        updateJsonbinInputJSONObj = new Updated_Jsonbin_inputDTO();
        UpdateJsonbin updatejsonbinerror = new UpdateJsonbin();
        updateJsonbinErrorResponse = updatejsonbinerror.updateJsonbinError(hostName, updateJsonbinInputJSONObj, jsonbinID );

        if (updateJsonbinErrorResponse.getStatusCode() != 400) {
            System.out.println("updateJsonbinError failed");
        }
        else
            System.out.println("updateJsonbinError passed");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify content for update jsonbin error
  @Then("^User should be able to verify the error in update jsonbin api using response body$")
  public void User_should_be_able_to_verify_the_error_in_update_jsonbin_using_response_body(
        ) throws Throwable {

        assertNotEquals(updateJsonbinErrorResponse.jsonPath().getString("message").toString(),"");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Delete Jsonbin API Service
  @When("^User should be able to delete jsonbin api service using host name \"([^\"]*)\"$")
  public void User_should_delete_jsonbin_api_service_using_host_name(
            String hostUrl) throws Throwable {

        hostName = hostUrl;
        DeleteJsonbin jsonbindelete = new DeleteJsonbin();
        deleteJsonbinResponse = jsonbindelete.deleteJsonbin(hostName, jsonbinIDNew);
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify deleted jsonbin api service
  @Then("^User should be able to verify the deleted jsonbin api using status code \"([^\"]*)\"$")
  public void User_should_be_able_to_verify_the_deleted_jsonbin_api_using_status_code(
            Integer StatusCode) throws Throwable {

        statusCode = StatusCode;
        if (deleteJsonbinResponse.getStatusCode() != statusCode) {
            System.out.println("deleteJsonbin failed");
        }
        else
            System.out.println("deleteJsonbin passed");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify error scenario for delete jsonbin api
  @When("^User gets error in deleting jsonbin api using host name \"([^\"]*)\"$")
  public void User_gets_error_in_deleting_jsonbin_api_using_host_name(
        String hostUrl) throws Throwable {

    hostName = hostUrl;
    DeleteJsonbin jsonbindeleteError = new DeleteJsonbin();
    deleteJsonbinErrorResponse = jsonbindeleteError.deleteJsonbinError(hostName, jsonbinIDNew);

      if (deleteJsonbinErrorResponse.getStatusCode() != 401) {
          System.out.println("deleteJsonbinError failed");
      }
      else
          System.out.println("deleteJsonbinError passed");
  }
// ========================================================================================================================================================================================================================================================================================================================================================
  // Verify content for update jsonbin error
  @Then("^User should be able to verify the error in delete jsonbin api using response body$")
  public void User_should_be_able_to_verify_the_error_in_delete_jsonbin_using_response_body(
        ) throws Throwable {

        assertNotEquals(deleteJsonbinErrorResponse.jsonPath().getString("message").toString(),"");
  }
}