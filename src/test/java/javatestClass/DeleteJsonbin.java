package javatestClass;


import io.restassured.response.Response;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import java.io.IOException;
import static io.restassured.RestAssured.given;

public class DeleteJsonbin {

	public Response deleteJsonbin(String hostname, String JsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/" + JsonbinId;

		Response deleteJsonbinResponse = given()
				                         .header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
		 								 .when()
									     .delete(baseURL);

		return deleteJsonbinResponse;
	}

	public Response deleteJsonbinError(String hostname, String JsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/" + JsonbinId;

		Response deleteJsonbinErrorResponse = given()
											  .when()
				                              .delete(baseURL);

		return deleteJsonbinErrorResponse;
	}
}
