package javatestClass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import org.json.JSONObject;


@JsonIgnoreProperties(ignoreUnknown = true)

public class Jsonbin_inputDTO {
  private String jsonToString;
  
  @Override
  @JsonValue
	public String toString() {
		
    	jsonToString= "{\r\n  \"sample\": \"Hello World\"\r\n}";
	  	JSONObject json = new JSONObject(jsonToString);
		return jsonToString;
	}

}
