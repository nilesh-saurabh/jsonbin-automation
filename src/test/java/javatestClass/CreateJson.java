package javatestClass;

import io.restassured.response.Response;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import java.io.IOException;
import static io.restassured.RestAssured.given;


public class CreateJson {
    public Response createJson(String hostname, Jsonbin_inputDTO createJsonbinRequestBody)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL;

		Response createJsonbinResponse = given()
										.contentType("application/json")
										.header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
										.body(createJsonbinRequestBody.toString()).when().post(baseURL);

		return createJsonbinResponse;	
	}

	public Response createJson_error(String hostname, Jsonbin_inputDTO createJsonbinRequestBody)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL;

		Response createJsonbinErrorResponse = given()
				.header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
				.body(createJsonbinRequestBody.toString()).when().post(baseURL);

		return createJsonbinErrorResponse;
	}


}
