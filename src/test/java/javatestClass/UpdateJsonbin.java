package javatestClass;


import io.restassured.response.Response;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import java.io.IOException;
import static io.restassured.RestAssured.given;

public class UpdateJsonbin {

	public Response updateJsonbin(String hostname, Updated_Jsonbin_inputDTO updateJsonbinRequestBody, String jsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/" + jsonbinId;

		Response updateJsonbinResponse = given()
										 .contentType("application/json")
				                         .header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
				                         .body(updateJsonbinRequestBody.toString())
				                         .when()
				                         .put(baseURL);

		return updateJsonbinResponse;
	}

	public Response updateJsonbinError(String hostname, Updated_Jsonbin_inputDTO updateJsonbinRequestBody, String jsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/" + jsonbinId;

		Response updateJsonbinErrorResponse = given()
				.header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
				.body(updateJsonbinRequestBody.toString())
				.when()
				.put(baseURL);

		return updateJsonbinErrorResponse;
	}

}
