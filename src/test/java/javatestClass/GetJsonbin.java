package javatestClass;

import io.restassured.response.Response;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class GetJsonbin {

	public Response getJsonbin(String hostname)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + "/v3";

		Response getJsonbinResponse = given().when().get(baseURL);

		return getJsonbinResponse;
	}

	public Response getJsonbinId(String hostname, String jsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/"+ jsonbinId;

		Response getJsonbinIdResponse = given()
								        .header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
										.when()
										.get(baseURL);

		return getJsonbinIdResponse;
	}

	public Response jsonbinLatest(String hostname, String jsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/" + jsonbinId + "/latest";

		Response getJsonbinLatestResponse = given()
				                            .header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
				                            .when()
				                            .get(baseURL);

		return getJsonbinLatestResponse;
	}

	public Response getJsonbinIdError(String hostname, String jsonbinId)
			throws ClientProtocolException,IOException,JSONException {

		String baseURL = hostname + Constants.JSONBIN_URL + "/"+ jsonbinId;

		Response getJsonbinIdErrorResponse = given()
											.header("X-Master-Key","$2b$10$XvLXNNDhCUMrhuSQmucy8OlO/x6F7wMH/p84nxIRCO3dROlphvOJa")
											.when()
											.get(baseURL);

		return getJsonbinIdErrorResponse;
	}

}
