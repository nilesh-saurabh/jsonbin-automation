package javatestClass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import org.json.JSONObject;


@JsonIgnoreProperties(ignoreUnknown = true)

public class Updated_Jsonbin_inputDTO {
	private String jsonToString;

	@Override
	@JsonValue
		public String toString() {

			jsonToString = "{\r\n  \"sample\": \"Hello New World\"\r\n}";
			JSONObject json = new JSONObject(jsonToString);
			return jsonToString;
		}
}

