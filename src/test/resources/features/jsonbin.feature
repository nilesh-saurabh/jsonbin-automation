Feature: To_create_read_update_and_delete_jsonbin
 @TC01 @Test
 Scenario Outline: JSONBIN_Create: To Create a jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User create jsonbin service with content "<CONTENT>" and using host name "<HOST_NAME>"
  Then User should be able to verify the created jsonbin in response body using content "<CONTENT>"
  Examples:
   | HOST_NAME              | CONTENT     |
   | https://api.jsonbin.io | Hello World |
##############################################################################################################################################
 @TC02 @Test
 Scenario Outline: JSONBIN_Read: To read a jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User read jsonbin service using host name "<HOST_NAME>"
  Then User should be able to verify the jsonbin response using response message
  And User should read jsonbin api using jsonbin Id and host name "<HOST_NAME>"
  And User should read jsonbin api using jsonbin Id created latest and with host name "<HOST_NAME>"
  Examples:
   | HOST_NAME              |  |
   | https://api.jsonbin.io |  |
###########################################################################################################################################
 @TC03 @Test
 Scenario Outline: JSONBIN_Update: To Update a jsonbin api service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User update jsonbin api with content and using host name "<HOST_NAME>"
  Then User should be able to verify the created jsonbin in response body using new request body as content "<CONTENT>"
  Examples:
   | HOST_NAME              | CONTENT         |  |  |
   | https://api.jsonbin.io | Hello New World |  |  |
##############################################################################################################################################
 @TC04 @Test
 Scenario Outline: JSONBIN_Create_Error: To get error scenario for create jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User gets error while creating jsonbin service with content "<CONTENT>" and using host name "<HOST_NAME>"
  Then User should be able to verify the error in created jsonbin api using response body
  Examples:
   | HOST_NAME              | CONTENT     |
   | https://api.jsonbin.io | Hello World |
###########################################################################################################################################
 @TC05 @Test
 Scenario Outline: JSONBIN_Read_Error: To fetch error scenario for read jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User gets error while reading jsonbin api using jsonbin Id "<JSONBIN_ID>" and host name "<HOST_NAME>"
  Then User should be able to verify the error in get jsonbin api using response body
  Examples:
   | HOST_NAME              | JSONBIN_ID               |
   | https://api.jsonbin.io | 607eefa1f909765deef87965 |
###########################################################################################################################################
 @TC06 @Test
 Scenario Outline: JSONBIN_Update_Error: To fetch error scenario for update jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User gets error in updating jsonbin api with content and using host name "<HOST_NAME>"
  Then User should be able to verify the error in update jsonbin api using response body
  Examples:
   | HOST_NAME              |  |
   | https://api.jsonbin.io |  |
###########################################################################################################################################
 @TC07 @Test
 Scenario Outline: JSONBIN_Delete: To Delete a jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User should be able to delete jsonbin api service using host name "<HOST_NAME>"
  Then User should be able to verify the deleted jsonbin api using status code "<STATUS_CODE>"
  Examples:
   | HOST_NAME              | STATUS_CODE |
   | https://api.jsonbin.io | 200         |
#############################################################################################################################################
 @TC08 @Test
 Scenario Outline: JSONBIN_Delete_Error: To fetch error scenario for deleting a jsonbin service.
  Given User should be able to hit jsonbin api with valid host name "<HOST_NAME>"
  When User gets error in deleting jsonbin api using host name "<HOST_NAME>"
  Then User should be able to verify the error in delete jsonbin api using response body
  Examples:
   | HOST_NAME              |  |
   | https://api.jsonbin.io |  |
#############################################################################################################################################