# Test Assesment

# *JSONBIN_API*

# *Introduction* 

This project can be used to test the functionality for Create, Update, Read and Delete jsonbin api from the list operation using Rest Assured with Cucumber (BDD Approach) in a fast and reliable way.

# *Preconditions*

1) Set your environment variables properly on your system.

# *Installation steps*

1) Clone this project in your local machine

2) Open the project in the IDE

# *Test Execution steps*

1) We have one runner file TestRunner_jsonbin. Just execute this file as junit.

# *Test Cases Covered*

I have covered positive and negative scenarios for all CRUD operations and also some more scenarios for "Read" service.
